'use strict'

module.exports = (req, res, next) => {
  const { email, password } = req.body
  if (!email || !password) {
    return res.status(412).json({
      message: 'Username e Password são obrigatórios!'
    })
  }
  next()
}
