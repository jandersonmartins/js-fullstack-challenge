'use strict'

const jwt = require('jsonwebtoken')

const generateToken = (payload, key, opts = {}) => new Promise((resolve, reject) => {
  jwt.sign(payload, key, opts, function (err, token) {
    if (err) {
      return reject(err)
    }
    resolve(token)
  })
})

const verifyToken = (token, key) => new Promise((resolve, reject) => {
  jwt.verify(token, key, (err, decoded) => {
    if (err) {
      return reject(err)
    }
    resolve(decoded)
  })
})

module.exports = { generateToken, verifyToken }
