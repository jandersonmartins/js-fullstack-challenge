'use strict'

const bcrypt = require('bcryptjs')

const isPassword = (password, encoded) => bcrypt.compare(password, encoded)

const encrypt = async password => {
  const strPassword = String(password)
  const salt = await bcrypt.genSalt()
  return bcrypt.hash(strPassword, salt)
}

module.exports = { isPassword, encrypt }
