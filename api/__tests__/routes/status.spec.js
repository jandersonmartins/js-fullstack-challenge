'use strict'

const request = require('supertest')
const app = require('../../app')
const { sequelize } = require('../../models')

describe('Route GET /status', () => {
  afterAll(() => sequelize.close())

  describe('status 200', () => {
    it('should return running status', async () => {
      const response = await request(app)
        .get('/status')
        .expect(200)
        .expect('Content-Type', /json/)

      expect(response.body).toEqual({ app: 'running' })
    })
  })
})
