'use strict'

const request = require('supertest')
const app = require('../../app')
const { sequelize, User } = require('../../models')
const { encrypt } = require('../../lib/password')
const { verifyToken } = require('../../lib/token')
const { getConfig } = require('../../config')

describe('Route auth', function () {
  let user

  beforeAll(async () => {
    await User.destroy({ where: {} })
    user = await User.create({
      name: 'User 1',
      email: 'user@email.com',
      password: await encrypt('123')
    })
  })

  afterAll(async () => {
    await User.destroy({ where: {} })
    await sequelize.close()
  })

  describe('POST /auth/login', function () {
    const url = '/auth/login'

    describe('status 200', function () {
      it('should return json with token when authentication is success', async () => {
        const response = await request(app)
          .post(url)
          .send({ email: 'user@email.com', password: '123' })
          .expect(200)
          .expect('Content-Type', /json/)

        const tokenDecoded = await verifyToken(response.body.token, getConfig().jwtKey)
        expect(tokenDecoded.id).toEqual(user.get('id'))
        expect(tokenDecoded.name).toEqual(user.get('name'))
      })
    })

    describe('status 401', function () {
      const assert401 = response =>
        expect(response.body.message).toEqual('Username ou senha incorreto(s)!')

      it('should return 401 json when username not exists', async function () {
        const response = await request(app)
          .post(url)
          .send({ email: 'unknown@email.com', password: '123' })
          .expect(401)
          .expect('Content-Type', /json/)

        assert401(response)
      })

      it('should return 401 when password is wrong', async function () {
        const response = await request(app)
          .post(url)
          .send({ email: 'user@email.com', password: 'wrong' })
          .expect(401)
          .expect('Content-Type', /json/)

        assert401(response)
      })
    })

    describe('status 412', function () {
      const assert412 = response =>
        expect(response.body.message).toEqual('Username e Password são obrigatórios!')

      it('should return 412 json when username and password is not defined', async function () {
        const response = await request(app)
          .post(url)
          .send({})
          .expect(412)
          .expect('Content-Type', /json/)

        assert412(response)
      })

      it('should return 412 json when username is not defined', async function () {
        const response = await request(app)
          .post(url)
          .send({ password: '' })
          .expect(412)
          .expect('Content-Type', /json/)

        assert412(response)
      })

      it('should return 412 json when password is not defined', async function () {
        const response = await request(app)
          .post(url)
          .send({ email: 'user@email.com' })
          .expect(412)
          .expect('Content-Type', /json/)

        assert412(response)
      })
    })
  })
})
