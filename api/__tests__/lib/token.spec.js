'use strict'

const { generateToken, verifyToken } = require('../../lib/token')

describe('lib/jwt module', () => {
  describe('#generateToken', () => {
    it('should generate a jwt token', async () => {
      const payload = { id: 1 }
      const key = 'abc'
      const token = await generateToken(payload, key)
      const actual = await verifyToken(token, key)
      expect(actual.id).toEqual(payload.id)
    })
  })

  describe('#verify', () => {
    describe('valid token', () => {
      it('should return decoded payload', async () => {
        const key = 'abc'
        const token = await generateToken({ id: 1 }, key)
        const decoded = await verifyToken(token, key)
        expect(decoded.id).toEqual(1)
      })
    })

    describe('invalid token', () => {
      it('should throws error when token is empty', async () => {
        await expect(verifyToken('', '123')).rejects.toThrow(
          'jwt must be provided'
        )
      })

      it('should throws error when is invalid token', async () => {
        await expect(verifyToken('alsdjainvalid', '123')).rejects.toThrow(
          'jwt malformed'
        )
      })

      it('should throws error when signature is wrong', async () => {
        const token = await generateToken({ id: 1 }, '123')
        await expect(verifyToken(`${token}invalid`, '123')).rejects.toThrow(
          'invalid signature'
        )
      })
    })
  })
})
