'use strict'

const { encrypt, isPassword } = require('../../lib/password')

describe('lib/password module', () => {
  describe('#isPassword', () => {
    it('should return true when is a correct password', async () => {
      const fakePass = await encrypt('123')
      const actual = await isPassword('123', fakePass)
      expect(actual).toBe(true)
    })

    it('should return false when password is wrong', async () => {
      const fakePass = await encrypt('admin123')
      const actual = await isPassword('wrong', fakePass)
      expect(actual).toBe(false)
    })
  })

  describe('#encrypt', () => {
    it('should generate encrypted password', async () => {
      const actual = await encrypt('123')
      // TODO: Mock bcryptjs
      expect(actual).toBeTruthy()
    })
  })
})
