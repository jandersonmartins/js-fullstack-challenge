'use strict'

const config = require('./config.json')

const getConfig = () => config[process.env.NODE_ENV || 'development']

module.exports = { getConfig }
