'use strict'

const { encrypt } = require('../lib/password')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const commonData = {
      password: await encrypt('123'),
      createdAt: new Date(),
      updatedAt: new Date()
    }
    return queryInterface.bulkInsert('users', [
      {
        name: 'Administrador',
        admin: 1,
        email: 'adm@email.com',
        ...commonData
      },
      {
        name: 'Programador 1',
        admin: 0,
        email: 'dev1@email.com',
        ...commonData
      },
      {
        name: 'Programador 2',
        admin: 0,
        email: 'dev2@email.com',
        ...commonData
      }
    ], {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {})
  }
}
