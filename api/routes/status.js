'uese strict'

const { Router } = require('express')
const { sequelize } = require('../models')
const router = Router()

router.get('/', async (req, res) => {
  await sequelize.authenticate()
  res.json({ app: 'running' })
})

module.exports = router
