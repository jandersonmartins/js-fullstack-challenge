'uese strict'

const { Router } = require('express')
const { User } = require('../models')
const authParamsMiddleware = require('../middlewares/auth-params')
const { isPassword } = require('../lib/password')
const { generateToken } = require('../lib/token')
const { getConfig } = require('../config')

const router = Router()

router.post('/login', authParamsMiddleware, async (req, res) => {
  const { email, password } = req.body
  const user = await User.findOne({
    where: {
      email
    }
  })

  if (!user) {
    return res.status(401).json({
      message: 'Username ou senha incorreto(s)!'
    })
  }

  const validPassword = await isPassword(password, user.get('password'))
  if (!validPassword) {
    return res.status(401).json({
      message: 'Username ou senha incorreto(s)!'
    })
  }

  const { jwtKey } = getConfig()
  const payload = {
    id: user.get('id'),
    name: user.get('name')
  }
  const token = await generateToken(payload, jwtKey)

  res.json({ token })
})

module.exports = router
