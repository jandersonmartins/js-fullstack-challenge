import { REQUEST_SESSION_LOGIN, RECEIVE_SESSION_TOKEN, RECEIVE_SESSION_ERROR, SESSION_LOADING, CLEAN_SESSION, SESSION_LOADED } from '../types/session'
import { request } from '../services/http'
import { storeSession } from '../services/session'
import { getSession, removeSession } from '../services/session'

export const sessionLoading = () => ({
  type: SESSION_LOADING
})

export const sessionLoaded = () => ({
  type: SESSION_LOADED
})

export const requestLogin = () => ({
  type: REQUEST_SESSION_LOGIN
})

export const receiveToken = token => ({
  type: RECEIVE_SESSION_TOKEN,
  token
})

export const receiveError = err => ({
  type: RECEIVE_SESSION_ERROR,
  message: err.message
})

export const cleanSession = () => ({
  type: CLEAN_SESSION
})

export const bootstrapSession = () => dispatch => {
  dispatch(sessionLoading())
  const token = getSession()
  const sessionAction = token ? receiveToken(token) : cleanSession()
  dispatch(sessionAction)
  dispatch(sessionLoaded())
}

export const login = ({ credentials, history }) =>
  async dispatch => {
    dispatch(requestLogin())
    try {
      const { data: { token } } = await request({
        url: 'auth/login',
        method: 'POST',
        data: credentials
      })
      await storeSession(token)
      dispatch(receiveToken(token))
      history.replace('/main')
    } catch (e) {
      dispatch(receiveError(e))
    }
  }

export const logout = history => dispatch => {
  removeSession()
  dispatch(cleanSession())
  history.replace('/login')
}
