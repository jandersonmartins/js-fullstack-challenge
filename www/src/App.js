import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import store from './store'
import LauncherContainer from './containers/LauncherContainer'

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <LauncherContainer />
    </BrowserRouter>
  </Provider>
)

export default App
