const SESSION = 'session:data'

export const getSession = () => localStorage.getItem(SESSION)

export const storeSession = token => localStorage.setItem(SESSION, token)

export const removeSession = () => localStorage.clear()
