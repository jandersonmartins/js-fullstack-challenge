import axios from 'axios'

const client = axios.create({
  // TODO: ENV
  baseURL: 'http://localhost:3001',
  headers: {
    'Content-Type': 'application/json'
  }
})

const reqSuccess = async config => {
  let authHeaders = {}
  // Pegar token
  return {
    ...config,
    headers: {
      ...config.headers,
      ...authHeaders
    }
  }
}

const responseError = async error => {
  if (error.response && error.response.data) {
    const msg = (typeof error.response.data === 'string')
      ? error.response.data
      : error.response.data.message
    return Promise.reject(new Error(msg))
  }
  return Promise.reject(error)
}

client.interceptors.request.use(reqSuccess, undefined)
client.interceptors.response.use(undefined, responseError)

const request = opts => client.request(opts)

export { request }
