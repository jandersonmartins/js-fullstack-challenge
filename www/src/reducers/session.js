import {
  REQUEST_SESSION_LOGIN,
  RECEIVE_SESSION_TOKEN,
  RECEIVE_SESSION_ERROR,
  SESSION_LOADING,
  SESSION_LOADED,
  CLEAN_SESSION
} from '../types/session'

const initialState = {
  initialized: false,
  loading: false,
  authenticated: false,
  token: null,
  errorMessage: null
}

const session = (state = initialState, action) => {
  switch (action.type) {
    case SESSION_LOADING:
      return {
        ...state,
        initialized: false
      }

    case SESSION_LOADED:
      return {
        ...state,
        initialized: true
      }

    case REQUEST_SESSION_LOGIN:
      return {
        ...state,
        errorMessage: null,
        loading: true
      }

    case RECEIVE_SESSION_TOKEN:
      return {
        ...state,
        errorMessage: null,
        loading: false,
        token: action.token,
        authenticated: true
      }

    case RECEIVE_SESSION_ERROR:
      return {
        ...state,
        loading: false,
        errorMessage: action.message,
        authenticated: false
      }

    case CLEAN_SESSION:
      return {
        ...initialState,
        initialized: true
      }

    default:
      return state
  }
}

export default session
