import { combineReducers } from 'redux'
import session from '../reducers/session'

const appReducer = combineReducers({
  session
})

const rootReducer = (state, action) => {
  if (action.type === 'RESET') {
    state = undefined
  }
  return appReducer(state, action)
}

export default rootReducer
