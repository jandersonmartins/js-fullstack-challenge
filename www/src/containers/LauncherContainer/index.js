import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Switch } from 'react-router-dom'
import ProtectedRoute from '../../components/ProtectedRoute'
import LoginContainer from '../LoginContainer'
import { bootstrapSession as bootstrapSessionAction } from '../../actions/session'
import MainContainer from '../MainContainer'

class LauncherContainer extends Component {
  componentDidMount () {
    this.props.bootstrapSession()
  }

  render () {
    if (!this.props.initialized) {
      return <h1>Carregando...</h1>
    }

    const { authenticated } = this.props

    return (
      <Switch>
        <ProtectedRoute
          path={['/', '/login']}
          exact
          onlyGuest
          authenticated={authenticated}
          component={LoginContainer}
        />

        <ProtectedRoute
          path="/main"
          authenticated={authenticated}
          component={MainContainer}
        />
      </Switch>
    )
  }
}

const mapStateToProps = ({
  session: {
    initialized,
    authenticated
  }
}) => ({
  initialized,
  authenticated
})

const mapDispatchToProps = dispatch => ({
  bootstrapSession: () => dispatch(
    bootstrapSessionAction()
  )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LauncherContainer)
