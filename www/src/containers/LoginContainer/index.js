import React from 'react'
import { connect } from 'react-redux'
import LoginForm from '../../components/LoginForm'
import { login as loginAction } from '../../actions/session'

const LoginContainer = ({
  loading,
  errorMessage,
  history,
  login
}) => {
  const handleOnSubmit = data => login({
    credentials: data,
    history
  })
  return (
    <LoginForm
      onSubmit={handleOnSubmit}
      loading={loading}
      errorMessage={errorMessage}
    />
  )
}

const mapStateToProps = ({
  session: {
    loading,
    errorMessage
  }
}) => ({
  loading,
  errorMessage
})

const mapDispatchToProps = dispatch => ({
  login: params => dispatch(loginAction(params))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer)
