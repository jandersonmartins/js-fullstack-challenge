import React from 'react'
import { connect } from 'react-redux'
import { Nav, Container } from 'react-bootstrap'
import { logout as logoutAction } from '../../actions/session'

const MainContainer = ({ logout, history }) => {
  const onSelect = selectedKey => {
    if (selectedKey === 'logout') {
      return logout(history)
    }
  }

  return (
    <>
      <Nav
        activeKey="/home"
        onSelect={onSelect}
      >
        <Nav.Item>
          <Nav.Link href="/home">Active</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="link-1">Link</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="logout">Sair</Nav.Link>
        </Nav.Item>
      </Nav>
      <Container>
        <h1>Main</h1>
      </Container>
    </>
  )
}

const mapDispatchToProps = dispatch => ({
  logout: history => dispatch(logoutAction(history))
})

export default connect(
  undefined,
  mapDispatchToProps
)(MainContainer)
