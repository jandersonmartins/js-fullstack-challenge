import React from 'react'
import { Route, Redirect } from 'react-router-dom'

const ProtectedRoute = ({
  component: Component,
  authenticated,
  onlyGuest,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props => {
        let redirect
        if (onlyGuest) {
          if (authenticated) {
            redirect = '/main'
          }
        } else {
          if (!authenticated) {
            redirect = '/login'
          }
        }
        if (redirect) {
          return <Redirect to={redirect} />
        }
        return <Component {...props} />
      }}
    />
  )
}

export default ProtectedRoute
