import React, { useState } from 'react'
import { Form, Button, Alert } from 'react-bootstrap'
import './style.css'

const LoginForm = ({
  onSubmit,
  loading,
  errorMessage
}) => {
  const [ email, setEmail ] = useState('')
  const [ password, setPassword ] = useState('')

  const handleOnSubmit = evt => {
    evt.preventDefault()
    if (!email || !password) {
      return
    }
    onSubmit({ email, password })
  }

  return (
    <div className="login-container">
      <Form
        className="login-form"
        onSubmit={handleOnSubmit}
      >
        <Form.Group controlId="formBasicEmail">
          <Form.Label>E-mail</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            value={email}
            onChange={evt => setEmail(evt.target.value)}
            required
          />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Senha</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={evt => setPassword(evt.target.value)}
            required
          />
        </Form.Group>

        {errorMessage && (
          <Alert variant="danger">
            {errorMessage}
          </Alert>
        )}

        <Button
          variant="primary"
          type="submit"
          disabled={loading}
        >
          Entrar
        </Button>
      </Form>
    </div>
  )
}

export default LoginForm
